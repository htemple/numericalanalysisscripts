"""
Hiram Temple
2020

This file holds several different functions for finding the roots of functions
 
 Newton's needs the derivative of your function
 Secant does not and seems to need less iterations. it also requires the first two x values of the function in the call
 Bisection requires a guess of where the roots are, in order to use it. If a and b are chosen from the same side of the root's value, it doesn't work.
 
 Each function prints a table
"""


"""
 f is the function we are finding the root of
 df is the first derivative of f
 x is our starting x_0
 nmax is our number of iterations we desire
"""
def newton(f, df, x, nmax):
    fx = f(x)
    dfx = None
    ratio = None
    
    print("NEWTONS")
    print("Iterations  ", "  x  ", "              func(x)")

    for n in range(1, nmax+1):

        dfx = df(x)

        if abs(dfx) < 1.1102230246251565e-16:
            #print("Small derivative")
            return

        ratio = fx/dfx
        x = x-ratio
        fx = f(x)

        print(n, "           ", x, "    ", fx)

        if abs(ratio) < 1.1102230246251565e-16:
            #print("Convergence")
            return


"""
 f is the function we are finding the root of
 
 x is our starting x_0
 nmax is our number of iterations we desire
"""
def secant(f, a, b, nmax):
    fa = f(a)
    fb = f(b)
    
    print("SECANT")
    print("Iterations", "|approximate zero, (x)", "|f(x)")
    
    if abs(fa) > abs(fb):
        #print("swapped 1")
        (a, b) = (b, a)
        (fa, fb) = (fb, fa)

    print(0, a, fa)
    print(1, b, fb)

    for n in range(2, nmax+1):
        if abs(fa) > abs(fb):
            #print("swapped 2")
            (a, b) = (b, a)
            (fa, fb) = (fb, fa)
        d = (b - a) / (fb - fa)
        b = a
        fb = fa
        d = d * fa
        if abs(d) < 1.1102230246251565e-16:
            #print("convergence")
            return
        a = a - d
        fa = f(a)
        print(n, a, fa)

#Bisection
"""
Bisection method requires a and b, which are sampled from either side of the value of the root
f is desired function
nmax is max iterations desired, if your root guess is close to your a and b, you may want a smaller nmax

"""
def bisection(f, a, b, nmax):

    fa = f(a)
    fb = f(b)

    if numpy.sign(fa) == numpy.sign(fb):
        print(a,fa,b,fb)
        print("same signs")
        return
    error = b-a
    for i in range(nmax+1):
        error = error/2
        c = a + error
        fc = f(c)
        print(i,c,fc,error)
        #if abs(error) < 1.1102230246251565e-16:
        if abs(error) < 10e-6:
            print("convergence!! Our Zero is at x = ", c)
            return
        if numpy.sign(fa) == numpy.sign(fc):
            a = c
            fa = fc
        else:
            b = c
            fb = fc
            
