"""
Hiram Temple
Runge-Kutta Methods
Runge-Kutta-Fehlberg Methods
These methods are for solving Initial Value Problems and are very accurate at
small values
Otherwise, Euler's and Taylor Order 2 methods are overall more accurate
"""


import math as m


#Runge-Kutta-Fehlburg 45
"""
func is a differential equation
t_0 is starting value for t
x_0 is starting evaluation of x(t)
t_f is the final t value
h_0 is the value for h that increments t
n_max is the number of successful iterations desired
e_max is maximum acceptable error
e_min is minimum
h_min is lowest possible h
h_max is max
 this function uses rkf() as a helper function and return a list of [t,x] pairs
"""
def rkf45(func, t_0, x_0, t_f, h_0, n_max, e_min, e_max, h_min, h_max):
    h = h_0
    t = t_0
    x = x_0
    n = 0
    points = [[t,x]]
    while n < n_max and t < t_f:
        if h < h_min:
            h = h_min
        elif (h > h_max):
            h = h_max
        (x_four,x_five) = rkf(func,t,x,h)
        e = abs(x_four - x_five)
        if e > e_max and h > h_min:#big error, reject and decrease step size
            h = h/2
        else:# small error, accept
            n += 1
            t += h
            x = x_five
            points.append([t,x])
            if e < e_min: #tiny error, increase step size
                h = h*2
    return points

#Runge-Kutta-Fehlberg
"""
func is a differential equation
t is the starting value of t
x is the evaluation of x(t) at starting t
h is the step amount that increments t
This function is a helper function for rkf45() and returns a tupele
"""
def rkf(func, t, x, h):
    k_1 = h*func(t,x)
    k_2 = h*func((t + (1/4)*h), (x + (1/4)*k_1))
    k_3 = h*func((t + (3/8)*h), (x + (3/32)*k_1 + (9/32)*k_2))
    k_4 = h*func((t + (12/13)*h), (x + (1932/2197)*k_1 - (7200/2197)*k_2 + (7296/2197)*k_3))
    k_5 = h*func((t + h), (x + (439/216)*k_1 - (8)*k_2 + (3680/513)*k_3 - (845/4104)*k_4))
    k_6 = h*func(t + (1/2)*h, (x - (8/27)*k_1 + 2*k_2 - (3544/2565)*k_3 + (1859/4104)*k_4 - (11/40)*k_5))
    x_four = x + (25/216)*k_1 + (1408/2565)*k_3 + (2197/4104)*k_4 - (1/5)*k_5
    x_five = x + (16/135)*k_1 + (6656/12825)*k_3 + (28561/56430)*k_4 - (9/50)*k_5 + (2/55)*k_6
    return (x_four, x_five) #function returns a tuple

#RK4 Method
"""
f is a differential function
t_0 is starting t value
x_0 is x evaluated at starting t value
t_f is final t value after n steps
h is step value for incrementing t
This function returns a list of [t,x] pairs
"""
def RK4(f, t_0, x_0, t_f, h):
    points = []
    t = t_0
    T = t + (1/2)*h
    x = x_0
    points.append([t,x])
    K1 = h *(f(t,x))
    K2 = h * f(T,x + .5*K1)
    K3 = h * (f(T,x + .5*K2))
    K4 = h * (f(t+h,x + K3 ))

    t += h

    while t <= t_f:
        #print ((1/6)*(K1+ 2*K2 + 2*K3 + K4))
        #print(x,"Teeeees",T)
        x = x + (1/6)*(K1 + (2*K2) + (2*K3) + K4)
        #print(x)
        points.append([t,x])
        t += h
    return points


#Euler's method
"""
input: func -> dx/dt from IVP
(t_0,x_0)-> initial value
t_f -> final time
h -> time step
output: points -> array of points for estimated solution of IVP
"""
def euler(func, t_0, x_0, t_f, h):
    t = t_0
    x = x_0
    points = [[t,x]] #declare dynamic array of size [n][2]
    while t <= t_f:
        x += h*func(t,x)
        t += h
        points.append([t,x])
    return points


"""
Taylor Method Order 2
f is a function f(t,x)
df is the derivative of f
t_0 is initial t
x_0 is initial x
t_f is final t value
h is step value
"""
def order2(f,df, t_0, x_0, t_f, h):
    points = []
    t = t_0
    x = x_0
    points.append([t,x])
    t += h
    while t <= t_f:
        x = (x + h*(f(t,x)) + (((h**2)/2) * (df(t,x))))
        #print(x)
        points.append([t,x])
        t += h
    return points


"""
f and df below are templates for helper functions for the Taylor Method Order 2 and Euler's method
or you can used your functions in the main function, called as: Order2(your_function,derivative_of_your_function, t_0,...)
x is x(t) or the derivative,dx/dt, of f(), it is used as an already evaluated value wtihin other methods
below are examples:

def f(t,x):
    # (-te^-t) + e^-t
    return (-x + m.exp(-t))


def df(t,x):
    # (-te^-t) + e^-t + e^-t
    return (-x + m.exp(-t) + m.exp(-t))
"""
