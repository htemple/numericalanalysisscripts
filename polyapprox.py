"""
Hiram Temple
2020

This file is for functions used for approximating nth degree polynomials

"""

"""
Horner's Algorithm(synthetic division) calculates an N-th degree polynomial with N additions and N multiplications. A naive approach would take N^2.
a = coefficients array for polynomial
r = the x value to approximate the function at.
returns an array of values that would be the solution of synthetic division
"""

def horner(a,r):
    n = len(a)-1

    b = [0]*(n+1)

    b[n-1] = a[n]

    for i in range(n-1,-1,-1):
        #print(i,(r*b[i]+a[i]))
        b[i-1] = a[i] + r*b[i]
       
    return b

