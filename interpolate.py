"""
Hiram Temple
2020


This file holds several methods of interpolation of polynomials and other functions, including spline interpolation
"""

import math


"""
coeff() and eval()
These two functions work together to interpolate a polynomial, coeff called first to calculate a set of divided difference coefficients to be used in the eval function

coeff():
vals - an array containing f(x) values
args - an array containing x values
n - is the number of x values
returns array holding divided differences

eval():
args - array containing x values
div_diff - array containing divided differences (output from coeff())
y - value to evaluate the polynomial at
n - number of x values
returns the value of the polynomial interpolated at argument y
"""
def coeff(vals,args,n):
    #n = len(vals)-1
    for j in range(1,n):
        for i in range(n-1, j-1, -1):
            vals[i] = (vals[i]- vals[i-1])/ (args[i]-args[i-j])
    return vals

def eval(args, div_diff, y, n):
    val = div_diff[n-1]
    for i in range(n-2, -1 ,-1):
        val = val*(y-args[i]) + div_diff[i]
    return val
    

"""
inter_derive() (better name TBD):

interpolates the value of a function's derivative at x
f - The function in question
x - the value at which we are interpolating
d - n x n array of 0's
n - number of iterations
returns an array if it converges. If not, returns n
"""
def inter_derive(f, x, d, n):
    h = 1
    
    for i in range(0, n):

        d[i][0] = ((f(x + h) - f(x - h)) / (2 * h))

        for j in range(1, i+1):

            d[i][j] = d[i][j-1] + (d[i][j-1] - d[i-1][j-1]) / (4**j - 1)

        if (i > 0) and (abs(d[i][i] - d[i-1][i-1]) < 10**(-6)): # checking against error term
            return i+1

        h = 0.5*h

    return n

"""
romberg():
Romberg method for computing a definite integral

f - original function
a - interval start [a,b]
b - interval end [a,b]
r - array of 0's to be filled by function
n - length of r
returns nothing, but fills r directly with values. r is to be printed as a 2D matrix to create a table of approximations as the function performs iterations, the most accurate approximations appearing furthest to the bottom right of the table.
In other words, the best approximation is the final element in the 2D array, r.
"""

def romberg(f,a,b,r,n):
    h = b-a
    r[0][0] = ((h/2)*(f(a) + f(b)))
    
    for i in range(1, n):
        h = h/2
        sum = 0

        for k in range(1, ((2**i)), 2):
            sum = sum + f(a+k*h)

        r[i][0] = (0.5*r[i-1][0] + (sum * h))

        for j in range(1, i+1):
            r[i][j] = (r[i][j-1] + (r[i][j-1] - r[i-1][j-1]) / ((4**j)-1))

"""
simpsons:
Simpsons rule is another method of approximating the definite integral of a function.
f - function
a - interval start [a,b]
b - interval end [a,b]
n - number of iterations. More iterations improves approximation, but iterations are wasted if the improvement is smaller than 10^(-6) 
returns the approximation to 6 decimal places
"""

def simpsons(f,a,b,n):
    h = ((b - a) / (2*n))
    x = [a]
    
    for i in range(1,2*n):
        x.append(a+(i*h))
        #print(a+(i*h))

    x.append(b)
    sum = 0

    for i in range(1,n+1):
        #print("I: ", i)
        sum = sum + f(x[((2*i)-1)])

    sum1 = sum*4
    sum = 0
    
    for i in range(1,n):
        #print("I2: ", i)
        sum = sum + f(x[2*i])

    sum2 = sum*2

    sum = sum1+sum2

    result = (h/3) * (f(a) + f(b) + sum)
    return round(result,6)

"""
linear_spline():

approximating a function trajectory using linear splines
the nodes (t[i],y[i]) are to be interpolated by a linear spline S
n - length of t[] and y[], or number of nodes
t - array of known x values (together with y[] create (x,y) nodes)
y - array of known y values (together with t[] create (x,y) nodes)
x - Value to approximate at
returns value of the spline at x, or S(x)
"""

def linear_spline(n, t, y, x):
    for i in range(n-1,-1,-1):
        if (x-t[i]) >= 0:
            return y[i] + (x-t[i])*((y[i+1]-y[i]) / (t[i+1]-t[i]))

"""
cubic_spline_coeff():

approximating a function trajectory using cubic splines
the nodes (t[i],y[i]) are to be interpolated by a cubic spline S
n - length of t[] and y[], or number of nodes
t - array of known x values (together with y[] create (x,y) nodes)
y - array of known y values (together with t[] create (x,y) nodes)

returns z, an array filled with values of S(t[i])
"""
def cubic_spline_coef(n, t, y):
    z = [0 for i in range(n)]
    h = [0 for i in range(n)]
    b = [0 for i in range(n)]
    u = [0 for i in range(n-1)]
    v = [0 for i in range(n-1)]
    
    for i in range(0, n-1):
        h[i] = t[i+1] - t[i]
        b[i] = (y[i+1] - y[i]) / h[i]

    u[0] = 2 * (h[0] + h[1])
    v[0] = 6 * (b[1] - b[0])

    for i in range(1, n-1):

        u[i] = 2 * (h[i+1] + h[i]) - (h[i]**2) / u[i-1]
        v[i] = 6 * (b[i+1] - b[i]) - h[i-1]*v[i-1] / u[i-1]

    z[n-1] = 0

    for i in range(n-2,0,-1):
        z[i] = (v[i-1] - h[i]*z[i+1]) / u[i-1]

    z[0] = 0
    return z
    
