"""
Hiram Temple
2020

matmath contains functions for linear algebra, such as solving a system of linear equations, as well as other matrix related analysis functions
"""


#Tri-Gauss
"""
This function uses the main diagonal, super diagonal and sub diagonal of a matrix of linear equations
a - sub diagonal
d - main diagonal
c - super diagonal
b - coefficients array
Returns an array of x values which fit the coefficients given in b
"""

def tri_gauss(a,d,c,b):

    n = (len(d)-1)
    x = []  #solution array

    for i in range(n+1):
        x.append(None)

    for i in range(2, n+1):
        xmult = a[i-1]/d[i-1]
        d[i] = (d[i] - (xmult*c[i-1]))
        b[i] = (b[i] - (xmult*b[i-1]))

    x[n-1] = (b[n-1]/d[n-1])

    for i in range(n-2, -1, -1):
        x[i] = ((b[i] - (c[i]*x[i+1])) / d[i])

    return x


"""
l2diff is a helper function for SOR
"""

def l2diff(x , y, length):
    sum = 0
    for i in range(0,length):
        sum += (x[i]-y[i])**2
    return math.sqrt(sum)

"""
SOR stands for Successive Over Relaxation, it is an imrovement on Jacobi and Gauss-Siedel methods for solving a diagonally dominant matrix of linear equations and is heavily
derived from the two predecessors

A - a matrix, or list of lists in Ax = b
b - result vector in Ax = b
x - variable vector x in the equation Ax = b
n - max iterations
d - minimum acceptable value of a diagonal element
e - error thresh hold stopping value
prints a table of iterations as it get closer to the real values of the solution vector for the SLE
no return value
"""
def SOR(A,b,x,n,d,e):
    diag = 0
    sum = 0
    kmax = 100
    for k in range(0,kmax):
        if k == 0:
            print("Initial   ", x)
            continue
        y = x[:]

        for i in range(0,n):
            
            sum = b[i]
            diag = A[i][i]
           
            if (abs(diag) < d):
                print("Diagonal element is too small")
                return
            for j in range(0, i):
                
                sum += -(A[i][j]*x[j])
            for j in range(i+1, n):
                
                sum += -(A[i][j]*x[j])
           
            x[i] = sum/diag
           
            x[i] = (1.1 * x[i]) + (.1 * y[i])

        print(k, "        ", x)


        if (l2diff(x, y, n) < e):
            print("||x-y|| < e", end = ": ->    ")
            print(k, "   ", x)
            return

    print("maximum iterations reached")
    return

"""
Naive Gaussian Elimination

Yet another method of solving an SLE, easier to use, but not necessarily optimal
a - Matrix, or list of lists representing A in Ax = b
b - solution vector in Ax = b
returns a vector of x values, {x_1,x_2,...,x_n} that solve the equations
"""
def naive_gauss(a, b):
    n = len(a)
    s = 0
    x = []
    
    for i in range(n + 1):
        x.append(0)

    for k in range(1, n):
        for i in range(k + 1, n + 1):
            xmult = a[i][k] / a[k][k]
            a[i][k] = xmult
            
            for j in range(k + 1, n + 1):
                a[i][j] -= a[i][j] - xmult * a[k][j]
            
            b[i] -= xmult * b[k]

    x[n] = b[n] / a[n][n]
    
    for i in range(n - 1, 2):
        s += b[i]
        
        for j in range(i + 1, n + 1):
            s += a[i][j] * x[j]
        
        x[i] = s / a[i][i]

    return x

